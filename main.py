import pandas as pd
import time

def deduplicate_csv(input_file, output_file, unique_attributes):
    start_time = time.time()

    df = pd.read_csv(input_file)

    df_deduplicated = df.drop_duplicates(subset=unique_attributes)

    df_deduplicated.to_csv(output_file, index=False)

    end_time = time.time()
    execution_time = end_time - start_time

    print(f"Deduplication process completed in {execution_time:.2f} seconds.")

if __name__ == "__main__":
    input_file_path = "/Users/levanmtchedlishvili/Desktop/techp/steam-200k.csv"
    output_file_path = "/Users/levanmtchedlishvili/Desktop/techp/deduplicated_steam.csv"

    unique_attributes = ["attribute1", "attribute2"]  

    deduplicate_csv(input_file_path, output_file_path, unique_attributes)
